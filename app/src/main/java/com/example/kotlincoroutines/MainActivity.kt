package com.example.kotlincoroutines

import android.os.Bundle
import android.os.PersistableBundle
import android.provider.Settings.Global
import android.util.Log
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.job
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import kotlinx.coroutines.withTimeout
import kotlin.system.measureTimeMillis

class MainActivity : AppCompatActivity() {
    val TAG = "SM_Coroutine thread"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //coroutine launch
        /* GlobalScope.launch {
             Log.d(TAG, "Hello from coroutine thread ${Thread.currentThread().name}")
         }
         Log.d(TAG, "Hello from main ${Thread.currentThread().name}")*/

        // Coroutine suspend function
     /*   GlobalScope.launch {
            val networkCallAnswer = networkCall()
            val networkCallAnswer2 = networkCall2()
            Log.d(TAG, networkCallAnswer)
            Log.d(TAG, networkCallAnswer2)
        }*/

        // Coroutines context
        /*val textView = findViewById<TextView>(R.id.tvDummy)
        GlobalScope.launch(Dispatchers.IO) {
            Log.d(TAG, "Hello from coroutine thread ${Thread.currentThread().name}")
            val answer = networkCall()
            withContext(Dispatchers.Main) {
                textView.text = answer
            }
        }*/

        // Coroutine runBlocking
        /*runBlocking {
            // RUn blocking blocks only main thread and can run
            // multiple threads parallel inside run blocking
            launch(Dispatchers.IO) {
                delay(3000L)
                Log.d(TAG, "End of thread1")
            }
            launch(Dispatchers.IO) {
                delay(3000L)
                Log.d(TAG, "End of thread2")
            }
            Log.d(TAG, "Start of run Blocking")
            delay(5000L)
            Log.d(TAG, "End of run Blocking")
        }
        Log.d(TAG, "After run Blocking")*/

        // Coroutine jobs: Coroutine returns the job
        /*val job = GlobalScope.launch(Dispatchers.Default){
            repeat(5){
                Log.d(TAG, "Continue still working...")
            }
        }

        runBlocking {
            // this job.join() function waits till coroutine finish
            job.join()
            Log.d(TAG, "Main thread continues...")

        }*/

        // Coroutine jobs cancel for long running tasks
       /* val job = GlobalScope.launch(Dispatchers.Default){
            Log.d(TAG, "Starting long running calculations...")

            // We can use withTimeout
            // It will cancel that thread automatically after given time
            withTimeout(500L){
                for(i in 30..40){
                    // Until we check isActive for thread it won't cancel for long running tasks.
                    if(isActive) {
                        Log.d(TAG, "Result for $i = ${fib(i)}")
                    }
                }
            }
            Log.d(TAG, "Main thread continues...")


            *//*  for(i in 30..40){
                  // Until we check isActive for thread it won't cancel for long running tasks.
                  if(isActive) {
                      Log.d(TAG, "Result for $i = ${fib(i)}")
                  }
              }*//*
        }*/
       /*runBlocking {
           delay(1000L)
           job.cancel()
           Log.d(TAG, "Main thread continues...")

       }*/

        // Async & Await
        GlobalScope.launch {
            val time = measureTimeMillis {
                val ans1 = async { networkCall()  }
                val ans2 = async { networkCall2() }
                Log.d(TAG, "ans1 = ${ans1.await()}")
                Log.d(TAG, "ans2 = ${ans2.await()}")
            }
            Log.d(TAG, "time taken = $time ms.")
        }

    }

    fun fib(n: Int):Long{
        return if(n == 0) 0
        else if( n==1 ) 1
        else fib(n-1) + fib(n-2)
    }

    suspend fun networkCall(): String {
        delay(3000L)
        return "This is the answer"
    }

    suspend fun networkCall2(): String {
        delay(3000L)
        return "This is the answer"
    }
}